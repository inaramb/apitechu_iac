var express = require('express');
var port = process.env.PORT || 3000;

var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());


var requestJson = require('request-json');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

var config = require('./config');
var VerifyToken = require('./verifyToken');

var baseMLabURL = config.mLabURL;


app.listen(port);
console.log("##############################")
console.log("API TechU IAC escuchando en el puerto " + port);

app.get("/apitechu/v1",
  function(req, res){
    console.log("##############################")
    console.log("GET /apitechu/v1");
    res.send({"msg" : "Hola desde APITechU IAC"});
});

app.get("/apitechu/v1/users",
  function(req, res){
    console.log("##############################")
    console.log("GET /apitechu/v1/users");

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    var filter = 'f={"_id":0, "last_access":0, "password":0}';

    httpClient.get("user?" + filter + "&" + config.mLabAPIKey,
      function (err, resMLab, body){
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
    });
});

app.post("/apitechu/v1/users",
  function(req, res){
    console.log("##############################")
    console.log("POST /apitechu/v1/users");
    var response = {};

    req.body.password = bcrypt.hashSync(req.body.password, 8); //Ciframos la password

    createUser (req.body,
      function (responseCreateUser, error, returnCode){
        if (!error){
          delete responseCreateUser.password;
          createAccount(responseCreateUser.id,
            function (data, errorCreateAccount){
              if (!errorCreateAccount) {
                response = data;
                res.status(201)
              } else {
                response = data.msg;
                res.status(data.returnCode);
              }
              res.send(responseCreateUser);
          });
        } else {
          res.status(returnCode);
          res.send(responseCreateUser);
        }
    });
});

app.post("/apitechu/v1/login",
  function (req, res){
    console.log("##############################")
    console.log("POST apitechu/v1/login");

    var msg = "Login incorrecto"

    var userEmail = req.body.email;
    var userPassword = req.body.password;

    var query = 'q={"email": "' + userEmail + '"}';
    var filter = 'f={"_id":0, "id":1, "password":1}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + query + "&" + filter + "&" + config.mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {"auth": false, "token": null}
          res.status(500);
        } else {
          if (body.length > 0) {
            var passwordIsValid = bcrypt.compareSync(req.body.password, body[0].password);
            if (!passwordIsValid){
              console.log("Password not valid");
              response = {"auth": false, "token": null}
              res.status(404);
            } else {
              var token = jwt.sign({ id: body[0].id }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
              });
              res.status(200);
              response = {"auth": true, "token": token}
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + query + "&" + config.mLabAPIKey, JSON.parse(putBody),
                function (errPut, resMLabPut, bodyPut){
              });
            }
          } else {
            console.log("Usuario no encontrado");
            response = {"auth": false, "token": null}
            res.status(404);
          }
        }
        res.send(response);
    });
});

app.delete('/apitechu/v1/users', VerifyToken,
  function(req, res, next) {
    console.log("##############################")
    console.log("DELETE /apitechu/v1/users");

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
    var query = 'q={"userid": ' + req.userId + '}';

    httpClient.get("account?" + query + "&" + config.mLabAPIKey,
        function (errMLabAccount, resMLab, bodyMLabAccount){
            if (errMLabAccount){
                response = {"msg" : "Error al borrar usuario"};
                res.status(500);
                res.send(response);
            } else {
                for (var i in bodyMLabAccount) {
                  httpClient.delete("account/" + bodyMLabAccount[i]._id.$oid + "?" + config.mLabAPIKey,
                          function (errMLabDeleteUser, resMLabDeleteAccount){
                              if (errMLabDeleteUser){
                                console.log("Error al borrar cuentas");
                              } else {
                                console.log("Cuenta " + bodyMLabAccount[i].iban + " borrada.");
                              }
                   });
                }
                query = 'q={"id": ' + req.userId + '}';
                httpClient.get("user?" + query + "&" + config.mLabAPIKey,
                    function (errMLabUser, resMLabUser, bodyMlabUser){
                        if (errMLabUser){
                            response = {"msg" : "Error al borrar usuario"};
                            res.status(500);
                            res.send(response);
                        } else {
                            console.log(bodyMlabUser[0]._id.$oid);
                            httpClient.delete("user/" + bodyMlabUser[0]._id.$oid + "?" + config.mLabAPIKey,
                                function (errMLabDeleteUser, resMLabDeleteUser){
                                    if (errMLabDeleteUser){
                                        response = {"msg" : "Error al borrar usuario"};
                                        res.status(500);
                                        res.send(response);
                                    } else {
                                        response = {"msg" : "Usuario borrado con exito"};
                                        res.status(200);
                                        res.send(response);
                                    }
                             });
                        }
                });
            }
    });
});

app.put('/apitechu/v1/users/password', VerifyToken,
  function(req, res, next) {
    console.log("##############################")
    console.log("PUT /apitechu/v1/users/password");

    getUserById(req.userId,
      function(data, err){
        if(err) { //Error al obtener usuario
          if (data.returnCode == 500){ //Error interno
            response = {"msg" : "Error al modificar contraseña"};
            res.status(500);
          } else {
            console.log("userId: " + req.userId);
            response = {"msg" : "Usuario no encontrado al modificar contraseña"};
            res.status(404);
          }
          res.send(response);
        } else {
          //delete data.password;
          delete req.body.email;
          var passwordIsValid = bcrypt.compareSync(req.body.oldpassword, data.password);
          if (!passwordIsValid){
            console.log("Password distinta");
            response = {"msg" : "La contraseña antigua no coincide"};
            res.status(404);
            res.send(response);
          } else {
            req.body.password = bcrypt.hashSync(req.body.newpassword, 8); //Ciframos la password

            var httpClient = requestJson.createClient(baseMLabURL);
            console.log("Cliente HTTP creado");
            var query = data._id.$oid;

            var putBody = '{"$set":{"password":"' + req.body.password + '"}}';

            httpClient.put("user/" + query + "?" + config.mLabAPIKey, JSON.parse(putBody),
              function (errPut, resMLabPut, bodyPut){});

            response = {"msg" : "Contraseña modificada con exito"};
            res.status(200);
            res.send(response);
          }
        }
      });
});

app.put('/apitechu/v1/users', VerifyToken,
  function(req, res, next) {
    console.log("##############################")
    console.log("PUT /apitechu/v1/users");

    getUserById(req.userId,
      function(data, err){
        if(err) { //Error al obtener usuario
          if (data.returnCode == 500){
            response = {"msg" : "Error al modificar datos del usuario"};
            res.status(500);
          } else {
            console.log("userId: " + req.userId);
            response = {"msg" : "Usuario no encontrado al modificar datos"};
            res.status(404);
          }
          res.send(response);
        } else {
          delete req.body.email;

          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente HTTP creado");
          var query = data._id.$oid;

          var putBody = '{"$set":{"first_name":"' + req.body.first_name + '", "last_name":"' + req.body.last_name + '", "avatar":"' + req.body.avatar + '", "gender":"' + req.body.gender + '"}}';

          console.log(putBody);

          data.first_name = req.body.first_name;
          data.last_name = req.body.last_name;
          data.gender = req.body.gender;
          data.avatar = req.body.avatar;

          httpClient.put("user/" + query + "?" + config.mLabAPIKey, JSON.parse(putBody),
            function (errPut, resMLabPut, bodyPut){});

          delete data._id;
          delete data.id;
          delete data.password;
          delete data.logged;
          res.status(200);
          res.send(data);
        }
      });
});

app.get('/apitechu/v1/me', VerifyToken,
  function(req, res, next) {
    console.log("##############################")
    console.log("GET /apitechu/v1/me");

    getUserById(req.userId,
      function(data, err){
        if(err) { //Error al obtener usuario
          if (data.returnCode == 500){ //Error interno
            response = {"msg" : "Error obteniendo usuarios"};
            res.status(500);
          } else {
            console.log("userId: " + req.userId);
            response = {"msg" : "Usuario no encontrado"};
            res.status(404);
          }
        } else {
          delete data._id;
          delete data.id;
          delete data.password;
          delete data.logged;
          response = data;
        }
        res.send(response);
      });
});

app.post("/apitechu/v1/logout", VerifyToken,
  function (req, res, next) {
    console.log("##############################")
    console.log("POST apitechu/v1/logout");

    getUserById(req.userId,
      function(data, err){
        if(err) { //Error al obtener usuario
          if (data.returnCode == 500){ //Error interno
            response = {"msg" : "Error obteniendo usuarios"};
            res.status(500);
          } else {
            response = {"msg" : "Usuario no encontrado"};
            res.status(404);
          }
        } else {
          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("Cliente HTTP creado");
          var query = 'q={"id": ' + data.id + '}';
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + config.mLabAPIKey, JSON.parse(putBody),
            function (errPut, resMLabPut, bodyPut){});
          response = {"auth": false, "token": null};
        }
        res.send(response);
      });
});

app.get("/apitechu/v1/users/accounts", VerifyToken,
  function(req, res, next){
    console.log("##############################")
    console.log("GET /apitechu/v1/users/accounts");

    var query = 'q={"userid":' + req.userId + '}';
    var filter = 'f={"_id":0, "accountid":1, "iban":1, "balance":1}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("account?" + query + "&" + filter + "&" + config.mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {"msg" : "Error obteniendo cuentas"};
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "El usuario no tiene cuentas asociadas"};
            res.status(404);
          }
        }
        res.send(response);
    });
});

app.get("/apitechu/v1/users/accounts/:accountid", VerifyToken,
  function(req, res, next){
    console.log("##############################")
    console.log("GET /apitechu/v1/users/accounts/:accountid");

    var query = 'q={"userid":' + req.userId + ',"accountid":' + req.params.accountid + '}';
    var filter = 'f={"_id":0, "userid":0, "accountid":0}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("account?" + query + "&" + filter + "&" + config.mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {"msg" : "Error obteniendo cuenta"};
          res.status(500);
        } else {
          if (body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Cuenta no encontrada"};
            res.status(404);
          }
        }
        res.send(response);
      });
});

app.post("/apitechu/v1/users/accounts", VerifyToken,
  function(req, res, next){
    console.log("##############################")
    console.log("GPOST /apitechu/v1/users/accounts");

    var response = {};
    createAccount(req.userId,
      function (data, error){
        if (!error) {
          response = data;
          res.status(201)
        } else {
          response = data.msg;
          res.status(data.returnCode);
        }
        res.send(response);
    });
});

app.post("/apitechu/v1/users/accounts/:accountid/movements", VerifyToken,
  function(req, res, next){
    console.log("##############################")
    console.log("POST /apitechu/v1/users/:userid/accounts/:accountid/movements");

    var movement = req.body;

    setMovementById(req.params.accountid, movement, function(data, error){
      var response = {};
      if (error){
        if (data.returnCode == 500){
          response = {"msg" : "Error obteniendo cuenta"}
          res.status(500);
        } else {
          response = {"msg" : "No existe la cuenta para ese usuario"};
          res.status(404);
        }
      }else {
        response = data;
        res.status(201);
      }
      res.send(response);
    });
});

app.post("/apitechu/v2/users/accounts/:accountid/transfer", VerifyToken,
  function(req, res, next){
    console.log("##############################")
    console.log("POST /apitechu/v2/users/accounts/:accountid/transferown");

    var oldBalanceOrig = 0;
    var accountIdOrig = req.params.accountid;
    var movementOrig = JSON.parse(JSON.stringify(req.body));

    var oldBalanceDest = 0;
    var accountIdDest;
    var movementDest = JSON.parse(JSON.stringify(req.body));

    var comment = req.body.comment;


    var query = 'q={"userid":' + req.userId + ',"accountid":' + accountIdOrig + '}';
    var filter = 'f={"_id":0, "userid":0, "accountid":0}';

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("account?" + query + "&" + filter + "&" + config.mLabAPIKey,
      function (err, resMLab, body){
        var response = {};
        if (err) {
          response = {"msg" : "Error obteniendo cuenta ordenante"};
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0) {
            response = body;
            oldBalanceOrig = body[0].balance;
            //Obtener oldBalanceDest
            var query = 'q={"iban":"' + movementDest.beneficiaryAccount + '"}';

            httpClient.get("account?" + query + "&" + config.mLabAPIKey,
              function (err, resMLab, bodyMLab){
                var response = {};
                if (err) {
                  response = {"msg" : "Error obteniendo cuenta beneficiaria"}
                  res.status(500);
                  res.send(response);
                } else {
                  if (bodyMLab.length > 0)
                  {
                    oldBalanceDest = bodyMLab[0].balance;
                    accountIdDest = bodyMLab[0].accountid;
                    //isFraud(279826.54, 279826.54, 492054.19, function(fraud, error){  //Fraud
                    //isFraud(2056.71, 120317.0, 0, function(fraud, error){               //Genuine
                    isFraud(movementOrig.amount, oldBalanceOrig, oldBalanceDest, function(fraud, error){
                      if (!error && !fraud) {
                        console.log("Genuina");
                        var responseIsNotFraud = {};

                        movementOrig.amount = movementOrig.amount*-1;
                        delete movementOrig.beneficiaryAccount;
                        setMovementById(accountIdOrig, movementOrig, function(data, error){});

                        delete movementDest.beneficiaryAccount;
                        setMovementById(accountIdDest, movementDest, function(data, error){});
                        responseIsNotFraud = {"msg" : "Transferencia realizada"};
                        res.status(201);
                        res.send(responseIsNotFraud);
                      } else if (!error) {
                        console.log("Fraude");
                        var responseIsFraud = {};

                        getUserById(req.userId,
                          function(data, err){
                            if(err) { //Error al obtener usuario
                              responseIsFraud = {"msg" : "Error obteniendo usuario"};
                              res.status(500);
                              res.send(responseIsFraud);
                            } else {
                              console.log('Enviando email');

                              var tokenJSON = {
                                amount : movementOrig.amount,
                                comment : comment,
                                accountIdDest : accountIdDest,
                                accountIdOrig : accountIdOrig
                              };

                              var tokenEmail = jwt.sign(tokenJSON, config.secret, {
                                expiresIn: 86400 // expires in 24 hours
                              });

                              var send = require('gmail-send')({
                                user: config.gmailAPI.user,
                                pass: config.gmailAPI.pass,
                                from: '"Techu Bank IAC" <techu.iac@gmail.com>',
                                to: data.email,

                                subject: 'Verificación de seguridad de transferencia',
                                html:    '<div class="bgded"><div id="pageintro" class="hoc clear"><article><h1 class="heading">Techu Bank IAC</h1><h3 class="heading">¿Has hecho tu la siguiente transferencia?</h3><p>Cuenta Destino: ' + movementOrig.beneficiaryAccount +'</p><p>Importe: ' + movementOrig.amount +' €</p><footer><a class="btn" href="' + config.transferVerificationURL + tokenEmail +'">Si</a></footer></article></div></div>'            // HTML
                              });

                              send({}, function (err, res) {
                                console.log('Mail enviado: callback err:', err, '; res:', res);
                              });
                              responseIsFraud = {"msg" : "Es necesario firmar la transferencia"}
                              res.status(307);
                              res.send(responseIsFraud);
                            }
                          });
                      }
                      else {
                        var responseIsFraudError = {};
                        console.log("Error al comprobar si la operacion es fraudulenta");
                        responseIsFraudError = {"msg" : "Error al comprobar si la operacion es fraudulenta"}
                        res.status(500);
                        res.send(responseIsFraudError);
                      }
                    });
                  } else {
                    response = {"msg" : "Cuenta destinataria no encontrada"};
                    res.status(404);
                    res.send(response);
                  }
                }
              });
          } else {
            response = {"msg" : "Cuenta ordenante no encontrada"};
            res.status(404);
            res.send(response);
          }
        }
      });
});

app.get("/apitechu/v1/users/accounts/transfer/:transferdata",
  function(req, res, next){
    console.log("##############################")
    console.log("GET /apitechu/v1/users/accounts/transfer/");

    jwt.verify(req.params.transferdata, config.secret, function(err, decoded) {
      if (err) {
        return res.status(500).send({ msg: 'Error al autenticar la transferencia' });
      } else {
        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Cliente HTTP creado");

        var query = 'q={"usedToken":"' + req.params.transferdata + '"}';

        httpClient.get("transferUsedToken?" + query + "&" + config.mLabAPIKey,
          function (err, resMLab, body){
            var response = {};
            if (err) {
              response = {"msg" : "Error al verificar transferencia"};
              res.status(500);
              res.send(response);
            } else {
              if (body.length > 0) {
                response = {"msg" : "La transferencia ya se ha ejecutado previamente"};
                res.status(400);
                res.send(response);
              } else {
                var transferData = decoded;

                var accountIdOrig = transferData.accountIdOrig;
                var movementOrig = {
                  amount: transferData.amount*-1,
                  comment: transferData.comment
                };
                setMovementById(accountIdOrig, movementOrig, function(data, error){});

                var accountIdDest = transferData.accountIdDest;
                var movementDest = {
                  amount: transferData.amount,
                  comment: transferData.comment
                };
                setMovementById(accountIdDest, movementDest, function(data, error){});

                var usedToke = {'usedToken': req.params.transferdata};
                httpClient.post("transferUsedToken?" + config.mLabAPIKey, usedToke,
                  function (errPut, resMLabPut, bodyPutMLab){
                    if (errPut) {
                      console.log("Error al almacenar Token Usado");
                    }
                });
                response = {"msg" : "Transferencia realizada"};
                res.status(201);
                res.send(response);
              }
            }
        });
    }
  });
});

app.post("/apitechu/v1/users/accounts/:accountid/transfer", VerifyToken,
  function(req, res, next){
    console.log("##############################")
    console.log("POST /apitechu/v1/users/accounts/:accountid/transfer");

    var movementBeneficiary = JSON.parse(JSON.stringify(req.body));
    var movementOrdering = JSON.parse(JSON.stringify(req.body));

    movementOrdering.amount = movementOrdering.amount*-1;
    delete movementOrdering.beneficiaryAccount;

    //Update ordering account
    setMovementById(req.params.accountid, movementOrdering, function(data, error){
      var response = {};
      if (error){
        if (data.returnCode == 500){
          response = {"msg" : "Error obteniendo cuenta"}
          res.status(500);
        } else {
          response = {"msg" : "No existe la cuenta para ese usuario"};
          res.status(404);
        }
        res.send(response);
      }else {
        //Find beneficiary accountId
        var query = 'q={"iban":"' + movementBeneficiary.beneficiaryAccount + '"}';
        console.log(query);

        var httpClient = requestJson.createClient(baseMLabURL);
        console.log("Cliente HTTP creado");

        httpClient.get("account?" + query + "&" + config.mLabAPIKey,
          function (err, resMLab, bodyMLab){
            var response = {};
            if (err) {
              response = {"msg" : "Error obteniendo cuenta beneficiaria"}
              res.status(500);
            } else {
              if (bodyMLab.length > 0) {
                delete movementBeneficiary.beneficiaryAccount;
                setMovementById(bodyMLab[0].accountid, movementBeneficiary, function(data, error){
                  if (error){
                    if (data.returnCode == 500){
                      response = {"msg" : "Error obteniendo cuenta beneficiria"}
                      res.status(500);
                      res.send(response);
                    } else {
                      response = {"msg" : "No existe la cuenta beneficiaria"};
                      res.status(404);
                      res.send(response);
                    }
                  }else {
                    response = {"msg" : "OK"};
                    res.status(201);
                    res.send(response);
                  }
                });
              } else {
                movementOrdering.amount = movementOrdering.amount * -1;
                setMovementById(req.params.accountid, movementOrdering, function(data, error){
                  var response = {};
                  if (!error){
                    response = {"msg" : "No existe la cuenta beneficiaria"};
                    res.status(404);
                    res.send(response);
                  }
                });
              }
            }
        });
      }
    });
});

function createAccount(userid, callback){
  getNextAccountId(
    function (nextAccountId, error){
      if (!error){
        var httpClient = requestJson.createClient(baseMLabURL);
        var response = {};
        //var faker = require('faker');
        var faker = require('faker/locale/es');

        try {
          var ValidIban = faker.finance.iban(false);
          console.log("ValidIBAN: " + ValidIban);
        } catch (e) {
          console.log("ERROR generateIbanForBank");
          console.log(e);
        }
        var account = {
          "accountid" : nextAccountId,
          "userid" : userid,
          "balance": 0,
          "iban" : ValidIban,
          "movement":[]
        }
        httpClient.post("account?" + config.mLabAPIKey, account,
          function (errPut, resMLabPut, bodyPutMLab){
            if (errPut) {
              response = {"returnCode" : 500, "msg" : "Error al crear cuenta"};
              return callback(response, true);
            } else {
              response = account;
              return callback(response, false);
            }
            res.send(response);
          });
      } else {
        response = {"returnCode" : 500, "msg" : "Error al crear cuenta"};
        return callback(response, true);
      }
    });
}

function setMovementById(accountId, movement, callback){
  var query = 'q={"accountid":' + accountId + '}';
  var upsert = 'u=false';

  var now = new Date()

  movement.type = ((movement.amount>=0) ? "ingreso" : "reintegro");
  movement.timestamp = now.toJSON();

  var bodyPutMLab = '{"$inc": {"balance": ' + movement.amount + ' }, "$push":{"movement":'+ JSON.stringify(movement) +'}}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("setMovementById - Cliente HTTP creado");

  httpClient.put("account?" + query + "&" + upsert + "&" + config.mLabAPIKey, JSON.parse(bodyPutMLab),
    function (err, resMLab, bodyMLab){
      var response = {};
      if (err) {
        response = {"returnCode" : 500, "msg" : "setMovementById - Error obteniendo cuenta"};
        return callback(response, true);
      } else {
        if (bodyMLab.n > 0) {
          return callback(response, false)
        } else {
          response = {"returnCode" : 404, "msg" : "Cuenta no existe"};
          return callback(response, true);
        }
      }
  });
}

function isFraud(amount, oldBalanceOrig, oldBalanceDest, callback){
  console.log("*******isFraud************");
  console.log("* Amount: " + amount);
  console.log("* oldBalanceOrig: " + oldBalanceOrig);
  console.log("* oldBalanceDest: " + oldBalanceDest);
  console.log("***************************");
  var httpClient = requestJson.createClient(config.isFraudAPIURL);
  console.log("isFraud - Cliente HTTP creado");

  var bodyPostFraud = {
    	"typeCashOut":1,
    	"typeDebit":0,
    	"typePayment":0,
    	"typeTransfer":0,
    	"amount":amount,
    	"oldBalanceOrig":oldBalanceOrig,
    	"newBalanceOrig":oldBalanceOrig - amount,
    	"oldBalanceDest":oldBalanceDest,
    	"newBalanceDest":oldBalanceDest + amount
    };

    httpClient.post("model", bodyPostFraud,
      function (errPost, resPost, bodyPost){
        var isFraude = 1;
        if (resPost.body.indexOf('genuine') > 0) {
          return callback(false, false);
        } else {
          return callback(true, false);
        }
    });
};

function getUserByEmail(email, callback){
  var response = {};
  var query = 'q={"email": "' + email + '"}';
  var filter = 'f={"_id":0}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("getUserByEmail - Cliente HTTP creado");

  httpClient.get("user?" + query + '&' + filter + '&' + config.mLabAPIKey,
    function (err, resMLab, body){
      if (err){
        response = {"returnCode" : 500, "msg" : "getUserByEmail - Error obteniendo usuarios"};
        return callback(response, true);
      } else {
        if (body.length > 0) {
          return callback(body[0], false)
        } else {
          response = {"returnCode" : 404, "msg" : "Usuario no existe"};
          return callback(response, true);
        }
      }
  });
}

function getUserById(id, callback){
  var response = {};
  var query = 'q={"id": ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("getUserById - Cliente HTTP creado");

  httpClient.get("user?" + query +  '&' + config.mLabAPIKey,
    function (err, resMLab, body){
      if (err){
        response = {"returnCode" : 500, "msg" : "getUserById - Error obteniendo usuarios"};
        return callback(response, true);
      } else {
        if (body.length > 0) {
          return callback(body[0], false)
        } else {
          response = {"returnCode" : 404, "msg" : "Usuario no existe"};
          return callback(response, true);
        }
      }
  });
}

function setUser(user, callback){
  var filterId = 'f={"_id":0, "id":1}';
  var sortId = 's={"id":-1}}';
  var limitId = 'l=1';
  var data = {};

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("createUser - Cliente HTTP creado");

  httpClient.get("user?" + filterId + '&' + sortId + '&' + limitId + '&'+ config.mLabAPIKey,
    function (errId, resMLab, bodyMaxId){
      if (errId) {
        data = {
          "returnCode" : 500,
          "msg" : "Error obteniendo usuarios"
        };
        return callback(data, true);
      } else {
        var iduser = -1;
        if (bodyMaxId.length > 0) {
          iduser = bodyMaxId[0].id;
        }
        user.id = iduser + 1;
        data = user;

        httpClient.post("user?" + config.mLabAPIKey, user,
          function (errPut, resMLabPut, bodyPutMLab){
            if (errPut) {
              data = {
                "returnCode" : 500,
                "msg" : "Error al crear el usuario"
              };
              return callback(data, true);
            } else {
              return callback(data, false);
            }
          }
        )
      }
    }
  )
}

function createUser (user, callback){
  getUserByEmail(user.email,
    function(data, err){
      if(err) { //Error al obtener usuario
        if (data.returnCode == 500){ //Error interno
          response = {"msg" : "500: Error obteniendo usuarios"};
          return callback(response, true, 500);
        } else {
          if (data.returnCode == 404){ //Usuario no encontrado
            setUser(user,
              function(dataInsert, errInsert){
                if (errInsert) { //Error al insertar el usuario
                  response = {"msg" : "Error al crear usuario"};
                  return callback(response, true, 500);
                } else {
                  response = dataInsert;
                  return callback(response, false, 201);
                }
              }
            );
          }
        }
      } else { //Usuario con mismo mail encontrado
        response = {
          "msg" : "Error, ya existe un usuario con el mismo email"
        }
        return callback(response, true, 403);
      }
    }
  );
}

function getNextAccountId(callback){
  var nextAccountId = 0;
  var filterId = 'f={"_id":0, "accountid":1}';
  var sortId = 's={"accountid":-1}}';
  var limitId = 'l=1';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("getNextAccountId - Cliente HTTP creado");

  httpClient.get("account?" + filterId + '&' + sortId + '&' + limitId + '&'+ config.mLabAPIKey,
    function (errId, resMLab, bodyMaxId){
      if (errId) {
        return callback(-1, true);
      } else {
        if (bodyMaxId.length > 0) {
          nextAccountId = bodyMaxId[0].accountid + 1;
        }
        console.log("getNextAccountId - nextAccountId: " + nextAccountId);
        return callback(nextAccountId, false);
      }
    }
  );
}
