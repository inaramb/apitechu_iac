var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var server = require('../server');

var should = chai.should();

var authToken;
var testUserId;

describe('Test endpoints API TechU IAC',
  function(){
    it('Prueba que la API est� escuchando',
      function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1')
        .end(
          function(err, res){
            console.log("Request has end");
            console.log(err);
            //console.log(res);
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde APITechU IAC");
            done();
          }
        )
      }
    ),
    it('Prueba que la API devuelve una lista de usuarios correctos ',
      function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/users')
        .end(
          function(err, res){
            console.log("Request has end");
            console.log(err);
            res.should.have.status(200);
            res.body.should.be.a("array");
            for (user of res.body){
              user.should.have.property('id');
              user.should.have.property('first_name');
              user.should.have.property('last_name');
              user.should.have.property('email');
              user.should.have.property('gender');
            }
            done();
          }
        )
      }
    ),
    it('Prueba que se puede crear un usuario',
      function(done) {
        chai.request('http://localhost:3000')
        .post('/apitechu/v1/users')
        .send({"first_name" : "test", "last_name" : "chaihttp", "email" : "test@chaihttp.com", "gender" : "Male", "password" : "test2017"})
        .end(
          function(err, res){
            console.log("Request has end");
            console.log(err);
            testUserId = res.body.id;
            console.log("UserId: " + testUserId);
            res.should.have.status(201);
            user.should.have.property('id');
            res.body.first_name.should.be.eql("test");
            res.body.last_name.should.be.eql("chaihttp");
            res.body.email.should.be.eql("test@chaihttp.com");
            done();
          }
        )
      }
    ),
    it('Prueba que se puede realizar login',
      function(done) {
        chai.request('http://localhost:3000')
        .post('/apitechu/v1/login')
        .send({"email" : "test@chaihttp.com", "password" : "test2017"})
        .end(
          function(err, res){
            console.log("Request has end");
            console.log(err);
            //console.log(res);
            authToken = res.body.token;
            res.should.have.status(200);
            res.body.auth.should.be.eql(true);
            res.body.should.have.property('token');
            done();
          }
        )
      }
    ),
    it('Prueba que se puede obtener cuentas de usuario',
      function(done) {
        chai.request('http://localhost:3000')
        .get('/apitechu/v1/users/accounts')
        .set('x-access-token', authToken)
        .end(
          function(err, res){
            console.log("Request has end");
            console.log(err);
            res.should.have.status(200);
            res.body.should.be.a("array");
            for (user of res.body){
              user.should.have.property('accountid');
              user.should.have.property('balance');
              user.should.have.property('iban');
            }
            done();
          }
        )
      }
    )
  }
);

describe('Borrar usuario',
  function(){
    it('Prueba que se puede borrar un usuario',
      function(done) {
        chai.request('http://localhost:3000')
        .delete('/apitechu/v1/users')
        .set('x-access-token', authToken)
        .end(
          function(err, res){
            console.log("Request has end");
            console.log(err);
            done();
          }
        )
      }
    )
  }
);
